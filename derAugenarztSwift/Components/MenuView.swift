//
//  menuView.swift
//  derAugenarztSwift
//
//  Created by Darius-George Oanea on 11/13/19.
//  Copyright © 2019 Darius-George Oanea. All rights reserved.
//

import UIKit
import Foundation


class MenuView: NibView {
    @IBOutlet var containerView: UIView!
    @IBOutlet var praxisButton: UIButton!
    @IBOutlet var arztButton: UIButton!
    @IBOutlet var fragenButton: UIButton!
    @IBOutlet var wertungButton: UIButton!
    @IBOutlet var mapButton: UIButton!
    override init(frame: CGRect) {
        super.init(frame: frame)
        customizeContainerView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        customizeContainerView()
    }

    func customizeContainerView() {
        containerView.backgroundColor = UIColor.clear
        Bundle.main.loadNibNamed("MenuView", owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = bounds
        autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}
