//
//  WertungViewController.swift
//  derAugenarztSwift
//
//  Created by Darius-George Oanea on 11/13/19.
//  Copyright © 2019 Darius-George Oanea. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

class WertungViewController: UIViewController, Storyboarded {
    
    @IBOutlet weak var menuView: MenuView!
    var disposeBag: DisposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startSubscriptions()
    }
    
    deinit {
        disposeSubscriptions()
    }
}

extension WertungViewController {
    func startSubscriptions() {
        menuView.praxisButton.rx
        .tap
            .subscribe(onNext: {[weak self] _ in
                MainCoordinator.shared.praxisSubscription()
                self?.dismiss(animated: true, completion: nil)
            }).disposed(by: disposeBag)
        
        menuView.arztButton.rx
            .tap
            .subscribe(onNext: {[weak self] _ in
                MainCoordinator.shared.arztSubscription()
                self?.dismiss(animated: true, completion: nil)
            }).disposed(by: disposeBag)
        
        menuView.fragenButton.rx
         .tap
             .subscribe(onNext: {[weak self] _ in
                 MainCoordinator.shared.fragenSubscription()
                 self?.dismiss(animated: true, completion: nil)
             }).disposed(by: disposeBag)
        
        menuView.mapButton.rx
        .tap
        .subscribe(onNext: {[weak self] _ in
            MainCoordinator.shared.mapSubscription()
            self?.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
        
    }
    
    func restartSubscriptions() {
        disposeSubscriptions()
        startSubscriptions()
    }
    
    func disposeSubscriptions() {
        disposeBag = DisposeBag()
    }
    
}

