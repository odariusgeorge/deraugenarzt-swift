//
//  Storyboarded.swift
//  derAugenarztSwift
//
//  Created by Darius-George Oanea on 11/13/19.
//  Copyright © 2019 Darius-George Oanea. All rights reserved.
//

import UIKit

protocol Storyboarded {
    static func instantiate() -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiate() -> Self {
    
        let id = String(describing: self)
    
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        return storyboard.instantiateViewController(identifier: id) as! Self

    }
}
