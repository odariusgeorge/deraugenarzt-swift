//
//  MainCoordinator.swift
//  derAugenarztSwift
//
//  Created by Darius-George Oanea on 11/13/19.
//  Copyright © 2019 Darius-George Oanea. All rights reserved.
//

import UIKit
protocol Coordinator {
    static var shared: MainCoordinator { get }
    
    var window: UIWindow? { get set }
    
    func sceneDelegateDidLoad(withWindow window: UIWindow?)
    
    func start()
}
class MainCoordinator: Coordinator {
    static var shared: MainCoordinator = MainCoordinator()
    var window: UIWindow?
    var navigationController = UINavigationController()
    
    private init() {
        navigationController.navigationBar.isHidden = true
        navigationController.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func start() {
        let vc = PraxisViewController.instantiate()
        navigationController.pushViewController(vc, animated: true)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
    }
    
    func praxisSubscription() {
        let vc = PraxisViewController.instantiate()
        navigationController.pushViewController(vc, animated: false)
    }
    
    func arztSubscription() {
        let vc = ArztViewController.instantiate()
        navigationController.pushViewController(vc, animated: false)
    }
    
    func fragenSubscription() {
        let vc = FragenViewController.instantiate()
        navigationController.pushViewController(vc, animated: false)
    }
    
    func wertungSubscription() {
        let vc = WertungViewController.instantiate()
        navigationController.pushViewController(vc, animated: false)
    }
    
    func mapSubscription() {
        let vc = MapViewController.instantiate()
        navigationController.pushViewController(vc, animated: false)
    }
    
    
    func sceneDelegateDidLoad(withWindow window: UIWindow?) {
        self.window = window
        
        start()
    }
}
